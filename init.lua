-- keymappings
vim.g.mapleader = " "
vim.api.nvim_set_keymap("n", "<C-/>", "gcc", { noremap = false })
vim.api.nvim_set_keymap("i", "<C-/>", "gcc", { noremap = false })
vim.api.nvim_set_keymap("n", "<leader>ep", ":Ex<CR>", { noremap = false })
vim.api.nvim_set_keymap("n", "<C-h>", "<C-w>h", { noremap = false })
vim.api.nvim_set_keymap("n", "<C-k>", "<C-w>k", { noremap = false })
vim.api.nvim_set_keymap("n", "<C-j>", "<C-w>j", { noremap = false })
vim.api.nvim_set_keymap("n", "<C-l>", "<C-w>l", { noremap = false })
vim.api.nvim_set_keymap("n", "<C-\\>", "<C-w>v", { noremap = false })
vim.api.nvim_set_keymap("t", "<Esc>", "<C-\\><C-n>", { noremap = false })

-- yanking highlight
vim.cmd("au TextYankPost * silent! lua vim.highlight.on_yank()")

-- no swap plz
vim.opt.swapfile = false
vim.opt.backup = false

-- searching
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- side number bar
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scl = 'yes'

-- tabbing
vim.opt.smarttab = true
vim.opt.sw = 4
vim.opt.ts = 4
vim.opt.sts = 4
vim.opt.si = true
vim.opt.et = false


-- good old commands
vim.api.nvim_create_user_command('Settings',
	function()
		CONFIG_FILE = vim.fn.stdpath('config')
		vim.cmd('cd '..CONFIG_FILE)
		vim.cmd('e '..CONFIG_FILE..'/init.lua')
	end,
	{ nargs = 0 }
)

function file_exists(path)
	local file = io.open(path, "rb")
	if file then file:close() end
	return file ~= nil
end

-- workspaces
function create_selection_box(title, lines, callback)
	local buf = vim.api.nvim_create_buf(false, true)
	local WIDTH = 100
	local HEIGHT = 20
	local opts = {
		relative = 'editor',
		width = 100,
		height = 20,
		col = math.floor((vim.o.columns - WIDTH) / 2),
		row = math.floor(((vim.o.lines - HEIGHT) / 2) - 1),
		anchor = 'NW',
		border = 'double',
		title_pos = 'center',
		title = title,
	}
	function select_line()
		-- NOTE: the current window will be our selection box
		local row, col = unpack(vim.api.nvim_win_get_cursor(0))
		vim.api.nvim_win_close(0, true)
		callback(row)
	end
	vim.api.nvim_buf_set_keymap(buf, "n", "<Esc>", ":q<CR>", { noremap = true })
	vim.keymap.set("n", "i", select_line, { noremap = true, buffer = buf })
	vim.keymap.set("n", "o", select_line, { noremap = true, buffer = buf })
	vim.keymap.set("n", "<Enter>", select_line, { noremap = true, buffer = buf })

	vim.api.nvim_open_win(buf, true, opts)
	vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)
end
WORKSPACE_FILE = vim.fn.stdpath('data')..'/workspaces.txt'
function get_workspace_files()
	function readall(filename)
		local fh = assert(io.open(filename, "rb"))
		local contents = assert(fh:read(_VERSION <= "Lua 5.2" and "*a" or "a"))
		fh:close()
		return contents
	end

	local available_workspace = {}
	local workspace_path = WORKSPACE_FILE
	if (file_exists(workspace_path)) then
		local data = readall(workspace_path)
		for line in data:gmatch '[^\n]+' do
			local val = string.find(line, '=')
			if (val ~= nil) then
				project_name = string.sub(line, 0, val - 1):gsub('%s+', "")
				project_path = vim.fn.expand(string.sub(line, val + 1):gsub('%s+', ""))
				available_workspace[project_name] = project_path
			end
		end
	end
	return available_workspace
end

perlind_workspace_context = {}
function workspace_setup(opts)
	if opts ~= nil then
		perlind_workspace_context = opts
	end
end

function workspace_exec(cmd)
	if (cmd == nil) then
		return
	end
	exec_ctx = perlind_workspace_context.exec[cmd]
	if (exec_ctx == nil) then
		return
	end

	perlind_workspace_context.last_exec = cmd
	dir = exec_ctx.dir
	if (dir == nil) then
		dir = perlind_workspace_context._workspace_dir
	else
		dir = string.gsub(dir, '${workspace}', perlind_workspace_context._workspace_dir)
		print(dir)
	end
	vim.cmd('TermExec cmd="cd '..dir..'"')
	vim.cmd('TermExec cmd="'..exec_ctx.cmd..'"')
end

function execute_popup()
	exec = perlind_workspace_context['exec']
	if (exec == nil) then
		print("No execs in the current workspace")
		return
	end
	local execute_flat = {}
	local execute_popup_list = {}
	for k, v in pairs(exec) do
		execute_flat[#execute_flat + 1] = k
		execute_popup_list[#execute_popup_list + 1] = k..": "..v.cmd
	end
	create_selection_box("Execute", execute_popup_list, function(idx)
		workspace_exec(execute_flat[idx])
	end)
end
function run_last_exec()
	if (perlind_workspace_context.last_exec == nil) then
		exec = perlind_workspace_context['exec']
		if (exec == nil) then
			print("No execs in the current workspace")
			return
		end
		execute_popup()
		return
	end
	workspace_exec(perlind_workspace_context.last_exec)
end

for _, exec_alias in pairs({"WorkspaceExec", "Wexec"}) do
	vim.api.nvim_create_user_command(exec_alias,
		function(opt)
			workspace_exec(opt.args)
		end,
		{
			nargs = 1,
			complete = function(_, _, _)
				local res = {}
				exec = perlind_workspace_context['exec']
				if (exec == nil) then
					return {}
				end
				for k, _ in pairs(exec) do
					res[#res + 1] = k
				end
				return res
			end,
		}
	)
end
vim.keymap.set("n", "<C-b>", run_last_exec, { noremap = false })
vim.keymap.set("n", "<F5>", execute_popup, { noremap = false })

function open_workspace(target_workspace)
	if (target_workspace == nil) then
		print("Invalid workspace")
		return
	end

	vim.cmd('cd '..target_workspace)
	vim.cmd('Ex '..target_workspace)

	local workspace_lua = target_workspace..'/workspace.lua'

	perlind_workspace_context = {}
	if file_exists(workspace_lua) then
		vim.cmd('so '..workspace_lua)
	end
	perlind_workspace_context['_workspace_dir'] = target_workspace
end

vim.api.nvim_create_user_command('Workspace',
	function(opt)
		target_workspace = get_workspace_files()[opt.args]
		open_workspace(target_workspace)
	end,
	{
		nargs = 1,
		complete = function(_, _, _)
			local res = {}
			for k, _ in pairs(get_workspace_files()) do
				res[#res + 1] = k
			end
			return res
		end,
	}
)

vim.api.nvim_create_user_command('WorkspacePaths',
	function(opt)
		vim.cmd('e '..WORKSPACE_FILE)
	end,
	{ nargs = 0, }
)

function select_workspace()
	local workspaces = get_workspace_files()
	local workspaces_flat = {}
	local workspaces_popup_list = {}
	for k, v in pairs(workspaces) do
		workspaces_flat[#workspaces_flat + 1] = v
		workspaces_popup_list[#workspaces_popup_list + 1] = k..': '..v
	end
	create_selection_box("Open Workspace", workspaces_popup_list, function(idx)
		open_workspace(workspaces_flat[idx])
	end)
end
vim.keymap.set("n", "<C-0>", select_workspace, { noremap = false })

-- packages
vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function(use)
	-- Packer can manage itself
	use('wbthomason/packer.nvim')
	use('ntpeters/vim-better-whitespace')
	use({ 'rose-pine/neovim', as = 'rose-pine' })
	use('tpope/vim-commentary')
	use('vim-airline/vim-airline')
	use('akinsho/toggleterm.nvim')
	use({
		'nvim-telescope/telescope.nvim', tag = '0.1.2',
		requires = { {'nvim-lua/plenary.nvim'} }
	})
	use({'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'})
	use({
		"neovim/nvim-lspconfig",
	})
end)

