local shell_to_use = vim.o.shell
if vim.loop.os_uname().sysname ~= "Windows_NT" then
	shell_to_use = 'fish'
else
	shell_to_use = 'powershell'
end
require("toggleterm").setup({
	persist_size = true,
	persist_mode = true,
	direction = 'vertical',
	size = 100,
	shell = shell_to_use,
})

